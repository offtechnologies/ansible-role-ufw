import os
import pytest
import testinfra.utils.ansible_runner


testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


@pytest.mark.parametrize('file', [
  ("/etc/default/ufw"),
  ("/etc/ansible/facts.d/ufw.fact")
])
def test_files(host, file):
    file = host.file(file)

    assert file.exists
    assert file.user == 'root'
    assert file.group == 'root'


@pytest.mark.parametrize('pkg', [
  'ufw'
])
def test_pkg(host, pkg):
    package = host.package(pkg)
    assert package.is_installed


def test_ufw_is_enabled(host):
    ufw = host.service("ufw")
    assert ufw.is_enabled
