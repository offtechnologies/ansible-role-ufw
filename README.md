ansible-role-ufw
=========
[![pipeline status](https://gitlab.com/offtechnologies/ansible-role-ufw/badges/master/pipeline.svg)](https://gitlab.com/offtechnologies/ansible-role-ufw/commits/master)

[offtechurl]: https://gitlab.com/offtechnologies
[![offtechnologies](https://gitlab.com/offtechnologies/logos/raw/master/logo100.png)][offtechurl]

Manages firewall (UFW) on Debian systems.

Requirements
------------
None

Role Variables
--------------

see `defaults/main.yml` and `vars/main.yml`

Dependencies
------------

None

Example Playbook
----------------

```yaml
---

- name: Configure dev system(s)
  hosts: all
  roles:
    - { role: ansible-role-ufw }
  vars:
    ufw_rules:
      - rule: allow
        to_port: 22
        protocol: tcp
```

License
-------

BSD
